export class Hilo {
    constructor(
        public id_hilo: number,
        public titulo: string,
        public comentario: string,
        public id_usuario: number,
        public archivo: string
    ){}
}
