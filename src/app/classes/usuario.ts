export class Usuario {
    constructor(
    	public id_usuario: number,
        public email: string,
        public password: string,
        public nombre: string,
        public bio: string,
        public imagen_perfil: string,
        public token_autologin: string
    ){}
}
