import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Router, RouterModule } from '@angular/router';
import {SingletonEventDispatcherService} from './services/common/singleton-event-dispatcher.service'

import {FileUploadModule} from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';


import { FileSelectDirective } from 'ng2-file-upload';

import { AppComponent } from './app.component';
import { FormLoginComponent } from './components/forms/form-login/form-login.component';
import { FormRegisterComponent } from './components/forms/form-register/form-register.component';
import { FormRememberComponent } from './components/forms/form-remember/form-remember.component';
import {HeaderComponent} from './components/header/header.component';

import { routes } from './app.routes';
import { ApplicationDataService } from './services/application-data.service';
import { PerfilComponent } from './components/details/perfil/perfil.component';
import { HomeComponent } from './components/lists/home/home.component';
import { RankingComponent } from './components/lists//ranking/ranking.component';
import { FormHiloComponent } from './components/forms/form-hilo/form-hilo.component';

import { SingleComponent } from './components/single/single.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FormLoginComponent,
    FormRememberComponent,
    FormRegisterComponent,
    PerfilComponent,
    HomeComponent,
    RankingComponent,
    FormHiloComponent,
    FileSelectDirective,
    SingleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    FileUploadModule,
    RouterModule.forRoot(routes),
    DialogModule,
    GrowlModule
  ],
  exports: [
    RouterModule
  ],
  providers: [ApplicationDataService, SingletonEventDispatcherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
