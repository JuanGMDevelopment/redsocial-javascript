import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Message} from 'primeng/primeng';
import { ApplicationDataService } from './services/application-data.service';
import { UsuariosService} from './services/usuarios.service'
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { SingletonEventDispatcherService } from './services/common/singleton-event-dispatcher.service';
import { HilosService} from './services/hilos.service';
import { Likes_HilosService} from './services/likes_hilos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UsuariosService, CookieService, Likes_HilosService, HilosService]
})
export class AppComponent implements OnInit{
  loaded_user = false;
  loaded_data = false;
  msgs: Message[] = [];

  constructor( private applicationDataService: ApplicationDataService
              , private router: Router
              , private hilosService: HilosService
              , private usuariosService: UsuariosService
              , private likesHilosService: Likes_HilosService
              , private  cookieService:CookieService
              , private singletonEventDispatcherService: SingletonEventDispatcherService) { }


  ngOnInit() {
    if (! this.applicationDataService.checkLogin())
    {
      this.router.navigate(['/form-login']);
    }
    /*//Comprobacion de autologin
    this.loaded_user = true;
    if (sessionStorage.getItem("email") !== null && sessionStorage.getItem("token_autologin") !== null)
    {
      this.loaded_user = false;
      this.usuariosService.autologin(sessionStorage.getItem("email"), sessionStorage.getItem("token_autologin")).subscribe(this.onAutoLogged);
    }*/
    this.singletonEventDispatcherService.SusbscribeEvent("init_load", this);
    this.singletonEventDispatcherService.SusbscribeEvent("showMessage", this);
  }


  dispatch(name: string, data: any)
  {
    if (name == "showMessage")
      this.msgs.push(data);
    if(name = "init_load"){
      this.hilosService.listado().subscribe(this.onHilosList);
      this.likesHilosService.listado_like_user().subscribe(this.onGetlikes);
      this.hilosService.listado_own(this.applicationDataService.usuario_logado.id_usuario).subscribe(this.onHilosPropiosList);
    }

  }

  onHilosList = (resultado : any) =>
  {
    if (resultado == null || (<Error>resultado != null && resultado.hasOwnProperty('mensaje')))
    {
      console.log("error al listar los hilos");
    }
    else{
      this.applicationDataService.hilos_publicos = resultado;
    }
  }


  onHilosPropiosList = (resultado : any) =>
  {
    if (resultado == null || (<Error>resultado != null && resultado.hasOwnProperty('mensaje')))
    {
      console.log("error al listar los hilos propios");
    }
    else{
      this.applicationDataService.hilos_propios = resultado;
    }
  }

  onGetlikes = (resultado: any) =>
  {    if (resultado == null || (<Error>resultado != null && resultado.hasOwnProperty('mensaje')))
        {
          console.log("error al listar likes");
        }
        else
        {
        this.applicationDataService.likes = resultado;

        this.singletonEventDispatcherService.ThrowEvent("data_loaded", this);
        }
  }
}
