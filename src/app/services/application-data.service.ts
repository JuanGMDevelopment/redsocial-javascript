import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import {Router} from '@angular/router'

import { Usuario } from '../classes/usuario';
import { Error } from '../classes/error';
import { Resultado } from '../classes/resultado';
import { Hilo } from '../classes/hilo';
import {Hilo_conUser} from "../classes/hilo_con_user";
import {Likes_Hilos} from '../classes/likes_hilos';

@Injectable()
export class ApplicationDataService {
  login = false;
  usuario_logado: Usuario = null;
  resultado_accion: Resultado = null;
  parameter_between_components: any = null;
  accion: string;
  Hilo_creado: Hilo=null;
  likes = null;
  hilos_publicos = null;
  hilos_propios = null;
  friends = null;


  ruta_a_servidor = "http://juan.redsocial.es";
  ruta_subida = this.ruta_a_servidor + "/data/hilos/subir_archivo";
  ruta_subida_perfil = this.ruta_a_servidor + "/data/usuarios/subir_imagen_perfil";


  constructor(private router : Router) { }

  click_menu(to){
    this.route("", to);
    $('.side-menu').toggleClass('active');
  }
  onVoid() {;}

  public route(from, to){
    this.router.navigate(['/'+ to])
  }

  public checkLogin() {
    if (this.usuario_logado == null)
    {
      return false;
    }
    else
    {
      return true;
    }
  }



  /*  CASTING SERVICE*/

  castJson(model: string, response: Response, is_array: boolean): any
  {
    if (response == null || response.text() == "[]")
      return {};

    let data = response.json();
    if (data.hasOwnProperty('ERROR'))
      return new Error(data.ERROR);

    switch (model)
    {
      //USUARIOS
      case "usuario":
        if (data.id_usuario > 0 || data.id_fb != "")
          return new Usuario(data.id_usuario, data.email, data.password, data.nombre, data.bio,data.imagen_perfil, data.token_autologin);
        return null;

      case "listado-usuarios":
        console.log(data)
        var usuarios ={}
        for(let data_ind of data) {
          if (data_ind.id_usuario > 0 )
            usuarios[data_ind.id_usuario1] =  new Usuario(data_ind.id_usuario, data_ind.email, data_ind.password, data_ind.nombre, data_ind.bio,data_ind.imagen_perfil, "");
        }
        console.log(usuarios);
        return usuarios;

      //HILOS
      case "hilo":
        let fecha;
        fecha = data.creation_time;
        let fecha_muro = fecha.split(" ", 2);
        if (data.status != "")
          return new Hilo_conUser(data.id_hilo, data.titulo, data.texto,  data.id_usuario, data.nombre_usuario, data.archivo, fecha_muro[0], 0, 0);
        return null;

      case "listado-hilos":
        var hilos ={}
        for(let data_ind of data) {
          let fecha;
          fecha = data_ind.creation_time;
          let fecha_muro = fecha.split(" ", 2);
          if (data_ind.id_hilo != "")
            hilos[data_ind.id_hilo] = new Hilo_conUser(data_ind.id_hilo, data_ind.titulo, data_ind.texto, data_ind.id_usuario, data_ind.nombre_usuario, data_ind.archivo, fecha_muro[0], data_ind.count_comments, data_ind.count_likes);
        }
        return hilos;
      //lIKES
      case "likes_frases":
        if (data.id_like > 0)
          return new Likes_Hilos( data.id_frase, data.id_usuario);
        return null;

      case "listado-likes":
        var likes_hilos = {};
        for (let data_ind of data)
          if (data_ind.id_hilo > 0)
            likes_hilos[data_ind.id_like] = new Likes_Hilos(data_ind.id_frase, data_ind.id_usuario);
        return likes_hilos;

      case "nuevo-like":
        if(data.id_like > 0 )
          return new Likes_Hilos( data.id_frase, data.id_usuario);
        return null;

      //GENERICO
      case "resultado":
        if (data.status != "")
          return new Resultado(data.status, data.filas_afectadas);
        return null;

      case "object":
        return data;

    }

  }

}
