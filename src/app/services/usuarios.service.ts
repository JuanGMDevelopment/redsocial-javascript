import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

import { ApplicationDataService } from '../services/application-data.service';

import { Usuario } from '../classes/usuario';
import { Resultado } from '../classes/resultado';
import { Hilo } from '../classes/hilo';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Injectable()
export class UsuariosService {

  constructor(private http:Http, private applicationDataService: ApplicationDataService, private cookieService:CookieService) { }

  /*LOGIN*/
  login(usuario: Usuario): Observable<any>
  {

    let body = JSON.stringify({'email': usuario.email, 'password': usuario.password});
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({
      headers: headers,
      withCredentials: true
    });
    return this.http.post(this.applicationDataService.ruta_a_servidor + "/data/usuarios/login", body, options)
      .map(res => this.applicationDataService.castJson('usuario', res, false));

  }

  /* AUTOLOGIN */
  autologin(email:String, token_autologin:String): Observable<any>
  {
    let body = JSON.stringify({'email': email, 'token_autologin': token_autologin});
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({
      headers: headers,
      withCredentials: true
    });

    return this.http.post(this.applicationDataService.ruta_a_servidor + "/data/usuarios/autologin", body, options)
      .map(res => this.applicationDataService.castJson('usuario', res, false));

  }

  /*REGISTRO*/
  register(usuario: Usuario): Observable<any>
  {
    let body = JSON.stringify({'email': usuario.email, 'password': usuario.password, 'nombre': usuario.nombre});
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({
      headers: headers,
      withCredentials: true
    });

    return this.http.post(this.applicationDataService.ruta_a_servidor + "/data/usuarios/register", body, options)
      .map(res => this.applicationDataService.castJson('usuario', res, false));

  }

  /*CAMBIAR CONTRASENA*/
  change_password(token:string, password: string): Observable<any>
  {
    let body = JSON.stringify({'token_remember': token, 'password': password});
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({
      headers: headers,
      withCredentials: true
    });

    return this.http.post(this.applicationDataService.ruta_a_servidor + "/data/usuarios/change_password", body, options)
      .map(res => this.applicationDataService.castJson('resultado', res, false));

  }

  /*RECORDAR CONTRASENA*/
  remember(usuario: Usuario): Observable<any>
  {
    let body = JSON.stringify({'email': usuario.email});
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({
      headers: headers,
      withCredentials: true
    });

    return this.http.post(this.applicationDataService.ruta_a_servidor + "/data/usuarios/remember", body, options)
      .map(res => this.applicationDataService.castJson('resultado', res, false));

  }


  edicion_usuario(usuario: Usuario): Observable<any> {
    //let params = {'email': usuario.email, 'password': usuario.password};

    let body = JSON.stringify({'id_usuario': usuario.id_usuario, 'email': usuario.email, 'nombre': usuario.nombre, 'bio': usuario.bio});

    let headers = new Headers({'Content-Type': 'text/plain'});
    let options = new RequestOptions({
      headers: headers,
      withCredentials: true
    });
    return this.http.post(this.applicationDataService.ruta_a_servidor + "/data/usuarios/edit", body, options)
      .map(res => this.applicationDataService.castJson('usuario', res, false));
  }

  logout(): Observable<any>
  {
    let body = JSON.stringify({});
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({
      headers: headers,
      withCredentials: true
    });

    //Quitamos el token autologin
    this.cookieService.remove("email");
    this.cookieService.remove("token_autologin");

    //Borramos el usuario en angular2
    this.applicationDataService.usuario_logado = null;

    return this.http.post(this.applicationDataService.ruta_a_servidor + "/data/admin/logout", body, options)
      .map(res => res);

  }
}
