/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ApplicationDataService } from './application-data.service';

describe('Service: ApplicationData', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApplicationDataService]
    });
  });

  it('should ...', inject([ApplicationDataService], (service: ApplicationDataService) => {
    expect(service).toBeTruthy();
  }));
});
