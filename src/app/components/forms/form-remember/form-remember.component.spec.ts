/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { FormRememberComponent } from './form-remember.component';

describe('FormRememberComponent', () => {
  let component: FormRememberComponent;
  let fixture: ComponentFixture<FormRememberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormRememberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRememberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
