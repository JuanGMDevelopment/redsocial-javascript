import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UsuariosService } from '../../../services/usuarios.service';
import { ApplicationDataService } from '../../../services/application-data.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';


import { Usuario } from '../../../classes/usuario';
import { Error } from '../../../classes/error';
import { Resultado } from '../../../classes/resultado';


@Component({
  selector: 'app-form-remember',
  templateUrl: './form-remember.component.html',
  styleUrls: ['./form-remember.component.css'],
  providers: [UsuariosService, CookieService]
})
export class FormRememberComponent implements OnInit {

  model = new Usuario(0, "", "", "", "", "", "");
  submitted = false;
  error = false;
  error_email_no_existe = false;

  constructor(private usuariosService: UsuariosService
            , private applicationDataService: ApplicationDataService
            , private router: Router
            , private cookieService:CookieService) { }

  ngOnInit() {
  }

  onSubmit() {

    this.submitted = true;
    this.usuariosService.remember(this.model).subscribe(this.onRemembered);
  }

  onRemembered = (resultado: any) => {

    if (resultado == null || (<Error>resultado != null && resultado.hasOwnProperty('mensaje')))
    {
        this.error = true;
        if (<Error>resultado != null && resultado.mensaje == "EMAIL_NOT_EXISTS")
            this.error_email_no_existe = true;
    }
    else
    {
        this.router.navigate(['/form-login']);
    }

    this.submitted = false;

  }
}
