import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';


import { HilosService } from '../../../services/hilos.service';
import { ApplicationDataService } from '../../../services/application-data.service';
import {SingletonEventDispatcherService} from '../../../services/common/singleton-event-dispatcher.service'


import { Error } from '../../../classes/error';

import { FileUploader } from 'ng2-file-upload';
import {Hilo} from "../../../classes/hilo";

@Component({
  selector: 'form-hilo',
  templateUrl: './form-hilo.component.html',
  styleUrls: ['./form-hilo.component.css'],
  providers: [HilosService, CookieService]
})
export class FormHiloComponent implements OnInit {
  model = new Hilo(0, "", "", 0, "");
  submitted = false;
  error = false;
  id_hilo;
  file_name;
  hilo_abierto;
  public uploader:FileUploader = null;
  hilo;


  constructor(private hilosService: HilosService
    , private cookieService:CookieService
    , private applicationDataService: ApplicationDataService
    , private router: Router
    , private route: ActivatedRoute
    , protected singletonEventDispatcherService: SingletonEventDispatcherService) { }

  @ViewChild('fileUpload') fileUploadModule;

  ngOnInit() {
    this.uploader = new FileUploader({url: this.applicationDataService.ruta_subida});
  }

  onAppFotosChange(event)
  { this.file_name = null;
    console.log(event.srcElement.files[0].name);
    this.file_name = event.srcElement.files[0].name;
  }

  onSubmit() {
    this.hilosService.publicarHilo(this.model).subscribe(this.onRegister);
  }


  onRegister = (resultado: any) => {
    if (resultado == null || (<Error>resultado != null && resultado.hasOwnProperty('mensaje')))
    {
      //TODO: Sacar un mensaje de error indicando que no se ha podido enviar las preguntas
      this.error = true;
    }
    else{
      this.hilo = resultado;
      this.uploader.setOptions({url: this.applicationDataService.ruta_subida + "/" + this.hilo.id_hilo});
      this.uploader.uploadAll();
      this.hilo.archivo = this.file_name;
      this.hilo_abierto= false;
      if(this.file_name == null)
        this.singletonEventDispatcherService.ThrowEvent("Hilos-changed", this.hilo);
    }


    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      var responsePath = JSON.parse(response);
      console.log(response, responsePath);// the url will be in the response
      this.file_name = null;
      this.singletonEventDispatcherService.ThrowEvent("Hilos-changed", this.hilo);
    };
  }


}
