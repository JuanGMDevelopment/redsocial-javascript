/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { FormHiloComponent } from './form-hilo.component';

describe('FormHiloComponent', () => {
  let component: FormHiloComponent;
  let fixture: ComponentFixture<FormHiloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormHiloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormHiloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
