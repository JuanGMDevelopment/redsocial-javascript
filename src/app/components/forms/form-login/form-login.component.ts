import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { UsuariosService } from '../../../services/usuarios.service';
import { ApplicationDataService } from '../../../services/application-data.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';

import { Usuario } from '../../../classes/usuario';
import { Error } from '../../../classes/error';
import { SingletonEventDispatcherService } from '../../../services/common/singleton-event-dispatcher.service';


@Component({
  selector: 'form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.css'],
  providers: [CookieService, UsuariosService]
})

export class FormLoginComponent implements OnInit {

  model = new Usuario(0, "", "", "", "", "", "");
  submitted = false;
  error = false;
  visible = false;


  constructor(private usuariosService: UsuariosService
            , private applicationDataService: ApplicationDataService
            , private router: Router
            , private cookieService:CookieService
            , private singletonEventDispatcherService: SingletonEventDispatcherService) { }




  ngOnInit() {
    this.applicationDataService.login = true;

  }

  onSubmit() {
    this.submitted = true;

    this.usuariosService.login(this.model).subscribe(this.onLogged);
  }

  onLogged = (resultado: any) => {
    if (resultado == null || (<Error>resultado != null && resultado.hasOwnProperty('mensaje')))
    {
      let message:any = {}
      message.severity = "error";
      message.summary = "LOGIN";
      message.detail = "LOGIN_KO";


      this.singletonEventDispatcherService.ThrowEvent("showMessage", message);
    }
    else
    {
        this.applicationDataService.usuario_logado = resultado;

       //Comprobamos autologin

      //sessionStorage.setItem("email", this.applicationDataService.usuario_logado.email);
      //sessionStorage.setItem("token_autologin", this.applicationDataService.usuario_logado.token_autologin);

      /*if(this.applicationDataService.usuario_logado.nombre=="" || this.applicationDataService.usuario_logado.nombre==null)
      this.router.navigate(['/profile']);*/

      this.applicationDataService.login = false;

      this.singletonEventDispatcherService.ThrowEvent("init_load",this);

      this.router.navigate(['/home']);


    }
    this.submitted = false;
  }
}
