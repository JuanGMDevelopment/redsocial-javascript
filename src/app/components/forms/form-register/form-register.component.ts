import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { UsuariosService } from '../../../services/usuarios.service';
import { ApplicationDataService } from '../../../services/application-data.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';

import { Usuario } from '../../../classes/usuario';
import { Error } from '../../../classes/error';
import {Message} from 'primeng/primeng';
import { SingletonEventDispatcherService } from '../../../services/common/singleton-event-dispatcher.service';

@Component({
  selector: 'form-register',
  templateUrl: './form-register.component.html',
  styleUrls: ['./form-register.component.css'],
  providers: [UsuariosService, SingletonEventDispatcherService, CookieService]
})
export class FormRegisterComponent implements OnInit {
  //@ViewChild('app_popup_register') popup_register;

  model = new Usuario(0, "", "", "", "", "", "");
  submitted = false;
  error = false;
  error_email_repetido = false;
  error_nombre_repetido = false;
  msgs: Message[] = [];

  constructor(private usuariosService: UsuariosService
            , private applicationDataService: ApplicationDataService
            , private singletonEventDispatcherService: SingletonEventDispatcherService
            , private router: Router
            , private cookieService: CookieService) { }

  ngOnInit() {
    /*if (this.applicationDataService.parameter_between_components != null)
    {
        this.model = this.applicationDataService.parameter_between_components;
        this.applicationDataService.parameter_between_components = null;
    }*/
    this.singletonEventDispatcherService.SusbscribeEvent("showMessage", this);
  }

  dispatch(name: string, data: any)
  {
    if (name == "showMessage")
      this.msgs.push(data);

  }

  onSubmit() {

    this.submitted = true;
    this.usuariosService.register(this.model).subscribe(this.onRegistered);
  }

  /*OnClickDetail()
  {
     //this.popup_register.visible = true;
  }*/

  onRegistered = (resultado: any) => {

    if (resultado == null || (<Error>resultado != null && resultado.hasOwnProperty('mensaje')))
    {
        this.error = true;
        let message:any = {}
        message.severity = "error";
        message.summary = "REGISTER";
        if (resultado.mensaje == "EMAIL_ALREADY_EXISTS")
        {
            this.error_email_repetido = true;
            message.detail = "EMAIL_YA_EXISTENTE";
        }

       if(resultado.mensaje == "NAME_USER_ALREADY_EXISTS"){
          this.error_nombre_repetido = true;
          message.detail = "NOMBRE_USUARIO_YA_EXISTENTE";
       }

      this.singletonEventDispatcherService.ThrowEvent("showMessage", message);
    }
    else
    {
        this.router.navigate(['/form-login']);
    }

    this.submitted = false;




      this.submitted = false;


  }

}
