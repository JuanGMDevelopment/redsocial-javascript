import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';

import { ApplicationDataService } from '../../../services/application-data.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { UsuariosService } from '../../../services/usuarios.service';
import { HilosService } from '../../../services/hilos.service';
import {SingletonEventDispatcherService} from '../../../services/common/singleton-event-dispatcher.service'
import {Hilo_conUser} from "../../../classes/hilo_con_user";
import {Likes_HilosService} from '../../../services/likes_hilos.service';


import { Usuario } from '../../../classes/usuario';
import { Error } from '../../../classes/error';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [CookieService, UsuariosService, HilosService, Likes_HilosService]
})
export class HomeComponent implements OnInit {

  @ViewChild('form_hilo') formHiloComponent;
  @ViewChild('app_single') app_single;
  error;
  hilos_publicos_array = null;
  friends_post = false;
  like_hilo=false;
  data_loaded =false;
  protected  single = false;

  constructor(  private applicationDataService: ApplicationDataService
    , private router: Router
    , private cookieService:CookieService
    , private usuariosService: UsuariosService
    , private hilosService: HilosService
    , private likesHilosService: Likes_HilosService
    , protected singletonEventDispatcherService: SingletonEventDispatcherService) { }


  ngOnInit() {
    if (! this.applicationDataService.checkLogin())
    {
      this.router.navigate(['/form-login']);
      return;
    }
    this.formHiloComponent.hilo_abierto = false;
    this.app_single.parent = this;
    this.singletonEventDispatcherService.SusbscribeEvent("Hilos-changed", this);
    this.singletonEventDispatcherService.SusbscribeEvent("data_loaded", this);

    /*if ($('.fancy-modal').length > 0) {
      $('.fancy-modal .close-fancy').click(function(event) {
        $('.fancy-modal').removeClass('active');
      });
    };*/

  }


  //ABRIR / CERRAR FORM HILO
  onclickCrearHilo(){
    //$('.fancy-modal#fancy-editar-horarios').addClass('active');
    this.formHiloComponent.hilo_abierto = true;
  }
  onclickCerrarCrearHilo(){
    //$('.fancy-modal#fancy-editar-horarios').removeClass('active');
    this.formHiloComponent.hilo_abierto = false;
  }
  //ELECCION LISTA POST
  seeAllPost(){
    this.friends_post= false;
  }
  seeFriendsPost(){
    this.friends_post= true;
  }

  //SINGLE
  onDetail(model){
    console.log(model);
    this.app_single.model = model;
    this.single= true;
  }


  //GESTION DE LIKES ---------------------------------------------------<
  like(hilo: Hilo_conUser) {
    //No distinguimos entre que esten logados o no para likes
    if (this.applicationDataService.likes[hilo.id_hilo] != null) {
      this.likesHilosService.dislike_click(this.applicationDataService.likes[hilo.id_hilo].id_hilo).subscribe(this.ondisLikeClick)
      delete this.applicationDataService.likes[hilo.id_hilo];
      this.editarContadorLikes(hilo.id_hilo, 1);
    }
    else {
      this.likesHilosService.like_click(hilo.id_hilo).subscribe(this.onLikeClick)
      //Damos un valor cualquiera para poner icono en dislike
      this.applicationDataService.likes[hilo.id_hilo] = "like_vacio";
      this.editarContadorLikes(hilo.id_hilo, 0);
    }
  }


  onLikeClick = (resultado: any) => {
    this.applicationDataService.likes[resultado.id_frase] = resultado;
  }

  ondisLikeClick = (resultado: any) => {
  }

  editarContadorLikes(id: number, clave: number) {
    for (let key of this.hilos_publicos_array) {


      if (key.id_hilo == id) {

        if (clave == 0)
          key.count_likes++;

        if (clave == 1)
          if (key.count_likes > 0)
            key.count_likes--;
      }
    }

  }
  //FIN GESTION DE LIKES ----------------------------------------------->





  //BORRAR POST
  model_a_borrar = null;
  onClickBorrar(model)
  { this.single = false;
    this.app_single.model= null;
    this.model_a_borrar = model;
    this.hilosService.borrado(model).subscribe(this.onDeleted);
  }

  onDeleted = (resultado: any) => {

    if (resultado == null || (<Error>resultado != null && resultado.hasOwnProperty('mensaje')))
    {
      this.error = true;
    }
    else
    {
      delete this.applicationDataService.hilos_publicos[this.model_a_borrar.id_hilo];
      this.hilos_publicos_array =[];
        for( let key in this.applicationDataService.hilos_publicos)
        {
          this.hilos_publicos_array.push(this.applicationDataService.hilos_publicos[key]);
        }
    }
  }

  //DISPARADOR EVENTOS
  dispatch(name:string, data:any)
  {  if (name == "Hilos-changed")
    {
    this.applicationDataService.hilos_publicos[data.id_hilo] = data;
    this.hilos_publicos_array.push(this.applicationDataService.hilos_publicos[data.id_hilo]);
    //$('.fancy-modal#fancy-editar-horarios').removeClass('active');
    }
    if (name == "data_loaded")
    {
      this.hilos_publicos_array = [];
      for (let key in this.applicationDataService.hilos_publicos) {
        this.hilos_publicos_array.push(this.applicationDataService.hilos_publicos[key]);
      }
      this.data_loaded =true;
    }

  }
}
