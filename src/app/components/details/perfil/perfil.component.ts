import { Component, OnInit, ViewChild } from '@angular/core';
import { ApplicationDataService } from '../../../services/application-data.service';
import { UsuariosService } from '../../../services/usuarios.service';
import { AmigosService} from '../../../services/amigos_service';


import { FileUploader } from 'ng2-file-upload';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Usuario } from '../../../classes/usuario';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
  providers: [UsuariosService, CookieService, AmigosService]
})
export class PerfilComponent implements OnInit {
    user = this.applicationDataService.usuario_logado;
    editar_bio = false;
    editar_profile_picture=false;
    submitted = false;
    submitted2 = true;
    error = false;
    model = new Usuario(this.user.id_usuario, this.user.email, "", this.user.nombre, "", "", "");
    public uploader:FileUploader = null;
    uploadFile: any;
    hilos_propios_array = null;
    amigos_propios_array = null;
    hilos_propios_visible = false;
    amigos_propios_visible = false;



  constructor(private applicationDataService: ApplicationDataService
              , private usuariosService: UsuariosService
              , private cookieService:CookieService
              , private amigosService: AmigosService) { }

  @ViewChild('fileUpload') fileUploadModule;

  ngOnInit() {
    this.uploader = new FileUploader({url: this.applicationDataService.ruta_subida});

    this.hilos_propios_array = [];
    for (let key in this.applicationDataService.hilos_propios) {
      this.hilos_propios_array.push(this.applicationDataService.hilos_publicos[key]);
    }

    this.amigosService.listado_own(this.applicationDataService.usuario_logado.id_usuario).subscribe(this.onFriendsReturn);
  }

  onFriendsReturn  = (resultado : any) =>{
    console.log(resultado);
    if (resultado == null || (<Error>resultado != null && resultado.hasOwnProperty('mensaje')))
    {
      console.log("error al listar los hilos propios");
    }
    else{
      this.applicationDataService.friends = resultado;
      this.amigos_propios_array = [];
      for (let key in this.applicationDataService.friends) {
        this.amigos_propios_array.push(this.applicationDataService.friends[key]);
      }

    }

  }

  editBio(){
    this.editar_bio=true;
  }

  editProfilePicture(){
    this.editar_profile_picture=true;
  }


  onSubmit(){
    this.submitted = false
    this.usuariosService.edicion_usuario(this.model).subscribe(this.onEditUser);
    this.editar_bio = false;
  }

  onEditUser = (resultado: any) => {
    if (resultado == null || (<Error>resultado != null && resultado.hasOwnProperty('mensaje'))) {
      this.error = true;
      this.editar_bio = true;
    }
    else {
      this.applicationDataService.usuario_logado = resultado;
      this.submitted = true;
    }
  }

  onSubmitImg(){
    this.submitted2= true;
    this.uploader.setOptions({url: this.applicationDataService.ruta_subida_perfil + "/" + this.applicationDataService.usuario_logado.id_usuario});
    this.uploader.uploadAll();
  }
  onAppFotosChange(event)
  {
    this.submitted2= false;
  }

  showHilosPropios(){
    this.hilos_propios_visible = true;
  }

  showFriendsPropios(){
    this.amigos_propios_visible = true;
  }

}
