import { ModuleWithProviders }  from '@angular/core';
import { Routes } from '@angular/router';

import { FormLoginComponent } from './components/forms/form-login/form-login.component';

import { FormRegisterComponent } from './components/forms/form-register/form-register.component';
import { FormRememberComponent } from './components/forms/form-remember/form-remember.component';

import { PerfilComponent } from './components/details/perfil/perfil.component';
import { HomeComponent } from './components/lists/home/home.component';
import { RankingComponent } from './components/lists//ranking/ranking.component';


// Route Configuration
export const routes: Routes = [
  { path: '', component: FormLoginComponent },
  { path: 'form-login', component: FormLoginComponent },
  { path: 'form-register', component: FormRegisterComponent },
  { path: 'form-remember', component:FormRememberComponent },
  { path: 'profile', component:PerfilComponent },
  { path: 'home', component:HomeComponent },
  { path: 'ranking', component:RankingComponent }
  
];


//{ path: 'emociones/:token', component: SelectorEmocionesComponent }