$(document).ready(function() {
  if ($('.main-content').length > 0) {
    var distance = $('.main-content').offset().top;        

    $(window).scroll(function() {
        if ( $(window).scrollTop() >= distance ) {
            $('.header').addClass('active');
        }else{
            $('.header').removeClass('active');
        }
    });   
  };
});
$(window).on('load', function(event) {
  if ($('.input-box').length > 0) {
    $('.input-box input').click(function(event) {
      $(this).parent().parent().parent().find('.input-box').removeClass('active');
      $(this).parent().addClass('active');

    });
  };
}); 