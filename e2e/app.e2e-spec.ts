import { RedSocialPage } from './app.po';

describe('red-social App', function() {
  let page: RedSocialPage;

  beforeEach(() => {
    page = new RedSocialPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
